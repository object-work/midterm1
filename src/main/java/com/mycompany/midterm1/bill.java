/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm1;

/**
 *
 * @author 66955
 */
public class bill {

    public String name;
    public double num;
    public char size;
    public char k;
    public double money = 0;

    public bill(String name, double num, char size, char k) {
        this.name = name;
        this.num = num;
        this.size = size;
        this.k = k;
    }

    public double money() {
        if (k == 'Y') {
            money += 5*num;
        } 
        else{
            money += 0;
        }
        
        if(size =='S'){
            money += 20*num;
        }
        else if(size =='M'){
            money += 30*num;
        }
        else if(size =='L'){
            money += 40*num;
        }
        return money;
    }

    public void print() {
        System.out.println("### NEW BILL ###");
        System.out.println("name = " + this.name);
        System.out.println("size = " + this.size);
        //k = ใส่เกี๊ยว
        System.out.println("K = " + this.k);
        //num = จำนวน
        System.out.println("num = " + this.num);
        System.out.println("price  = " + this.money);

    }

}
